/** @jsx h */
import {h} from 'preact';
import {HandlerContext} from '$fresh/src/server/types.ts';
import {RequestWithSession} from 'utils/types.ts';

export const handler = (req: RequestWithSession, _ctx: HandlerContext): Response => {
    const redirect = req.session!.user ? '/app' : '/login'
    return Response.redirect(new URL(req.url).origin + redirect)
}
