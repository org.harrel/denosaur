import {MiddlewareHandlerContext} from '$fresh/server.ts';
import {bindSession, setToken} from 'utils/session.ts';

export async function handler(req: Request, ctx: MiddlewareHandlerContext) {
    const {token} = bindSession(req)
    const res = await ctx.next()
    return setToken(res, token)
}