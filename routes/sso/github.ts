import {HandlerContext, Handlers} from '$fresh/src/server/types.ts';
import {gitHubApi} from 'utils/github.ts';
import {RequestWithSession} from 'utils/types.ts';
import {db} from 'utils/database.ts';

const AUTH_STATE_ATTR = 'AUTH_STATE_ATTR'

export const handler: Handlers = {
    async GET(req: RequestWithSession, _ctx: HandlerContext) {
        const url = new URL(req.url);
        const code = url.searchParams.get('code');
        if (!code) {
            return Response.redirect(new URL(req.url).origin + '/login', 302)
        }

        const state = url.searchParams.get('state');
        const authState = req.session!.getAttribute(AUTH_STATE_ATTR)
        req.session!.removeAttribute(AUTH_STATE_ATTR)
        if (state !== authState) {
            console.warn('state param has changed')
            return Response.redirect(new URL(req.url).origin + '/login', 302)
        }

        const token = await gitHubApi.getAccessToken(code)
        const user = await gitHubApi.getUserData(token)
        req.session!.user = user;

        const dbKey = {type: user.type, id: user.id}
        const dbUser = await db.users.findOne(dbKey)
        const date = new Date().toISOString()
        if (!dbUser) {
            await db.users.insertOne({...user, creationDate: date, lastLoginDate: date})
        } else {
            await db.users.updateOne(dbKey, {lastLoginDate: date})
        }
        return Response.redirect(new URL(req.url).origin, 302)
    },

    POST(req: RequestWithSession, _ctx: HandlerContext) {
        const clientId = Deno.env.get('GITHUB_CLIENT_ID')
        if (!clientId) {
            throw new Error('GITHUB_CLIENT_ID is not set')
        }

        const authState = crypto.randomUUID()
        req.session!.setAttribute(AUTH_STATE_ATTR, authState)
        const url = new URL('https://github.com/login/oauth/authorize');
        url.searchParams.set('client_id', clientId);
        url.searchParams.set('redirect_uri', req.url);
        url.searchParams.set('state', authState);
        return Response.redirect(url, 302);
    }
}
