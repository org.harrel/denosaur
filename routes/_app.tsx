/** @jsx h */
import {Component, Fragment, h} from 'preact';
import {asset, Head} from '$fresh/runtime.ts';
import {AppProps} from '$fresh/src/server/types.ts';

export default function App(props: AppProps) {
    const {Component} = props
    return (
        <Fragment>
            <Head>
                <title>Denosaur</title>
                <link rel="stylesheet" href={asset('/milligram.css')}/>
                <link rel="stylesheet" href={asset('/style.css')}/>
            </Head>
            <body>
            <Component/>
            </body>
        </Fragment>
    )
}