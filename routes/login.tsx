/** @jsx h */
import {h} from 'preact';
import {HandlerContext, PageProps} from '$fresh/src/server/types.ts';
import {RequestWithSession} from 'utils/types.ts';

export const handler = (req: RequestWithSession, ctx: HandlerContext): Response => {
    if (req.session!.user) {
        return Response.redirect(new URL(req.url).origin + '/app')
    }
    return ctx.render();
}

export default function LoginPage(props: PageProps) {
    return (
        <div class="center-container">
            <div class="login-tile">
                <h1>Sign in</h1>
                <form action="/sso/github" method="post">
                    <button>Login with github</button>
                </form>
            </div>
        </div>
    )
}