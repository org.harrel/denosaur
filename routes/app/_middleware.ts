import {MiddlewareHandlerContext} from '$fresh/server.ts';
import {RequestWithSession} from 'utils/types.ts';

export function handler(req: RequestWithSession, ctx: MiddlewareHandlerContext) {
    if (!req.session!.user) {
        return Response.redirect(new URL(req.url).origin + '/login')
    }
    return ctx.next();
}