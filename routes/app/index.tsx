/** @jsx h */
import {Fragment, h} from 'preact';
import {HandlerContext, PageProps} from '$fresh/src/server/types.ts';
import NavBar from 'components/NavBar.tsx';
import {RequestWithSession} from 'utils/types.ts';

export const handler = (req: RequestWithSession, ctx: HandlerContext) => {
    return ctx.render({user: req.session!.user});
}

export default function AppPage(props: PageProps) {
    return (
        <Fragment>
            <NavBar user={props.data.user}/>
            <div class="container">
                <h1>APP</h1>
            </div>
        </Fragment>
    )
}