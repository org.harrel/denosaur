// deno-lint-ignore-file no-explicit-any
import {getCookies, setCookie} from '$std/http/cookie.ts';
import {createHash} from '$std/hash/mod.ts';
import {RequestWithSession, Session, User} from 'utils/types.ts';
import {db, Session as PersistedSession} from 'utils/database.ts';
import {lessThanOrEqual, moreThan} from '$aloe/lib/helpers.ts';

const SESSION_TIME = Number.parseInt(Deno.env.get('SESSION_TIME')!)
const ctx = new Map<string, Session>()
loadPersistedSessions().then((count) => console.log(`Loaded ${count} persisted sessions`))

export const bindSession = (req: Request): { token: string, session: Session } => {
    const token = getCookies(req.headers)['denosaur_token'] || crypto.randomUUID()
    const sessionId = hash(token)
    let session = ctx.get(sessionId)
    if (!session) {
        session = SessionImpl.createNew(sessionId)
        ctx.set(sessionId, session)
    } else {
        session.refresh()
    }
    (req as RequestWithSession).session = session
    return {token, session}
}

export const setToken = (res: Response, token: string): Response => {
    const newHeaders = new Headers(res.headers)
    setCookie(newHeaders, {
        name: 'denosaur_token',
        value: token,
        path: '/',
        sameSite: 'Lax',
        maxAge: 60 * 15,
        httpOnly: true
    })
    return new Response(res.body, {
        headers: newHeaders,
        status: res.status,
        statusText: res.statusText
    })
}

const hash = (data: string): string => {
    const hash = createHash('sha256')
    hash.update(data)
    return hash.toString()
}

async function loadPersistedSessions() {
    const now = new Date().getTime()
    const persisted = await db.sessions.findMany({expiryDate: moreThan(now)})
    await db.sessions.deleteMany({expiryDate: lessThanOrEqual(now)})
    await Promise.all(persisted.map(p => SessionImpl.createFromPersisted(p).then(s => ctx.set(s.id, s))))
    return persisted.length
}

class SessionImpl implements Session {
    readonly id: string
    readonly #attributes: Map<string, any>
    expiryDate: Date
    #user?: User

    private constructor(id: string, attributes: Map<string, any>, expiryDate: Date, user?: User) {
        this.id = id
        this.#attributes = attributes
        this.expiryDate = expiryDate
        this.#user = user
    }

    static createNew(id: string): SessionImpl {
        const date = new Date()
        date.setMinutes(date.getMinutes() + SESSION_TIME)
        return new SessionImpl(id, new Map<string, any>(), date)
    }

    static async createFromPersisted(persisted: PersistedSession): Promise<SessionImpl> {
        const date = new Date()
        date.setTime(persisted.expiryDate)
        const user = await db.users.findOne(persisted.userId)
        return new SessionImpl(persisted.id, new Map<string, any>(), date, user ?? undefined)
    }

    get user() {
        return this.#user
    }

    set user(user) {
        this.#user = user
        if (!user) {
            this.#attributes.clear()
            db.sessions.deleteOne({id: this.id})
        } else {
            this.#persist({userId: {type: user.type, id: user.id}})
        }
    }

    refresh() {
        const date = new Date()
        date.setMinutes(date.getMinutes() + SESSION_TIME)
        this.expiryDate = date
        this.#persist({expiryDate: date.getTime()})
    }

    getAttribute(key: string) {
        return this.#attributes.get(key)
    }

    setAttribute(key: string, value: any) {
        this.#attributes.set(key, value)
    }

    removeAttribute(key: string) {
        this.#attributes.delete(key)
    }

    #persist(data: Partial<PersistedSession>) {
        if (!this.#user) {
            return
        }

        const sessionKey = {id: this.id}
        const newSession = {
            id: this.id,
            expiryDate: this.expiryDate.getTime(),
            userId: {type: this.#user.type, id: this.#user.id},
            ...data
        }
        db.sessions.findOne(sessionKey).then(r => {
            if (r) {
                return db.sessions.updateOne(sessionKey, newSession)
            } else {
                return db.sessions.insertOne(newSession)
            }
        })
    }
}

