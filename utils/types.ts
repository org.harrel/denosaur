// deno-lint-ignore-file no-explicit-any
export interface Session {
    readonly id: string
    readonly expiryDate: Date
    user?: User
    refresh(): void
    getAttribute(key: string): any
    setAttribute(key: string, value: any): void
    removeAttribute(key:string): void
}

export interface RequestWithSession extends Request {
    session?: Session
}

export type IdpProvider = 'github' | 'google'

export interface User {
    readonly id: number
    readonly type: IdpProvider
    readonly login: string
    readonly avatarUrl: string
    readonly profileUrl: string
}