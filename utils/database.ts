import {Database, Document} from '$aloe/mod.ts';
import {IdpProvider, User as _User} from 'utils/types.ts'

export interface User extends _User {
    type: IdpProvider
    creationDate: string
    lastLoginDate: string
}

export interface UserId extends Document {
    type: IdpProvider
    id: number
}

export interface Session {
    id: string
    expiryDate: number
    userId: UserId
}

class DB {
    readonly users = new Database<User>('./aloe/users.json')
    readonly sessions = new Database<Session>('./aloe/sessions.json')
}

export const db = new DB()
