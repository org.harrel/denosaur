import {User} from 'utils/types.ts';

export class GitHubApi {
    async getAccessToken(code: string) {
        const response = await fetch(
            "https://github.com/login/oauth/access_token",
            {
                method: "POST",
                body: JSON.stringify({
                    client_id: Deno.env.get('GITHUB_CLIENT_ID'),
                    client_secret: Deno.env.get('GITHUB_CLIENT_SECRET'),
                    code,
                }),
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            }
        );
        if (!response.ok) {
            throw new Error(await response.text());
        }
        const data = await response.json();
        const accessToken = data["access_token"];
        if (typeof accessToken !== "string") {
            throw new Error("Access token was not a string.");
        }
        return accessToken;
    }

    async getUserData(accessToken: string): Promise<User> {
        const response = await fetch("https://api.github.com/user", {
            headers: {
                Authorization: `token ${accessToken}`,
            },
        });
        if (!response.ok) {
            throw new Error(await response.text());
        }
        const userData = await response.json();
        return {
            type: 'github',
            id: userData.id,
            login: userData.login,
            avatarUrl: userData.avatar_url,
            profileUrl: userData.html_url
        }
    }
}

export const gitHubApi = new GitHubApi();